define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'p123payment',
                component: 'TBA_P123Payment/js/view/payment/method-renderer/P123Payment'
            }
        );
        return Component.extend({});
    }
);