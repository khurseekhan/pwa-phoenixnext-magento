<?php
namespace TBA\LOBOT\Controller\LobotHelper;
//use \TBA\LOBOT\Controller\LobotHelper\LobotConstants;

class LobotComfirmOrderParser implements \TBA\LOBOT\ParserInterface
{
    //protected $lobotConstants;

    public function __construct(
        //LobotConstants $lobotConstants,
        \Magento\Framework\App\Action\Context $context)
	{
        //$this->lobotConstants = $lobotConstants;
	    parent::__construct($context);
	}
    
    public function LoadData($fileNameList) {
        $dataPods = array();
        foreach ($fileNameList as $fileName)
        {
            $fileData = $this->openFiles($fileName);
            
            $fileData = "";
            foreach ($fileData() as $line) {
              $fileData .= $line;
            }
            $dataBeans = $this->readData($fileData);
            array_push($dataPods,$dataBeans);
        }
    }
    
    private function OpenFiles($fileName) {
        //$filePath = $this->lobotConstants->LOCAL_TMP_INTERFACE_DIR . $fileName;
        $filePath = '/tmpLobotInterface/'.$fileName;
        $file = fopen($filePath,"r");
        if (!$file)
        {
            die('file does not exist or cannot be opened');
        }
        while (($line = fgets($file)) !== false) 
        {
            yield $line;
        }
        fclose($file);
        return $line;
    }
    
    private function ReadData($fileData)
    {
      $dataBeans = array();
      $dataLines = splitNewLine($fileData);
        for ($i = 0; $i < count($dataLines); $i++)
        {
            $REC = substr($dataLines[$i],0,0);
            $KEY_INF = substr($dataLines[$i],0,0);
            $HCH_DPY_NO = substr($dataLines[$i], 0, 10);
            $NKA_DPY_NO = substr($dataLines[$i], 10,10);
            $NKA_DPY_MEISA_NO = substr($dataLines[$i], 20,6);
            $HCH_DPY_MEISAI = substr($dataLines[$i], 26,5);
            $SHN_COD = substr($dataLines[$i], 31,18);
            $DATA_KBN = substr($dataLines[$i], 49,3);
            $LOT_NO = substr($dataLines[$i], 52,15);
            $KOKI_NO = substr($dataLines[$i], 68,0);
            $NKA_YMD = substr($dataLines[$i], 67,8);
            $SSKI_KYK_PLT_COD = substr($dataLines[$i], 75,10);
            $PLANT = substr($dataLines[$i], 85,10);
            $HOK_BSY = substr($dataLines[$i], 89,4);
            $SYM_KGN = substr($dataLines[$i], 93,8);
            $NKA_SU = substr($dataLines[$i], 101,0);
            $NUM_NKA_SU = substr($dataLines[$i], 101,13);
            $NKA_SU_TUI = substr($dataLines[$i], 114,3);
            $NHN_DPY_NO = substr($dataLines[$i], 117,16);
            $bean  = array(
                'REC'=>$REC,
                'KEY_INF'=>$KEY_INF,
                'HCH_DPY_NO'=>$HCH_DPY_NO,
                'NKA_DPY_NO'=>$NKA_DPY_NO,
                'NKA_DPY_MEISA_NO'=>$NKA_DPY_MEISA_NO,
                'HCH_DPY_MEISAI'=>$HCH_DPY_MEISAI,
                'SHN_COD'=>$SHN_COD,$DATA_KBN,
                'LOT_NO'=>$LOT_NO,
                'KOKI_NO'=>$KOKI_NO,
                'NKA_YMD'=>$NKA_YMD,
                'SSKI_KYK_PLT_COD'=>$SSKI_KYK_PLT_COD,
                'SSKI_KYK_PLT_COD'=>$SSKI_KYK_PLT_COD,
                'PLANT'=>$PLANT,
                'HOK_BSY'=>$HOK_BSY,
                'SYM_KGN'=>$SYM_KGN,
                'NKA_SU'=>$NKA_SU,
                'NUM_NKA_SU'=>$NUM_NKA_SU,
                'NKA_SU_TUI'=>$NKA_SU_TUI,
                'NHN_DPY_NO'=>$NHN_DPY_NO);
            array_push($dataBeans,$bean);
      }
      return $dataBeans;
    }
}

