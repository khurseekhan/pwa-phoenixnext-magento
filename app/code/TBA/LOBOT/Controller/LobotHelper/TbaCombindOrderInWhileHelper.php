<?php
namespace TBA\LOBOT\Controller\LobotHelper;
use \Psr\Log\LoggerInterface;
use \TBA\LOBOT\Controller\LobotHelper\LobotOrderTsvBuilder;

class TbaCombindOrderInWhileHelper
{
    protected $logger;
    protected $lobotOrderTsvBuilder;
    
    public function __construct(
        LobotOrderTsvBuilder $lobotOrderTsvBuilder,
        LoggerInterface $logger) {
        $this->lobotOrderTsvBuilder = $lobotOrderTsvBuilder;
        $this->logger = $logger;
    }

    public function CombineShipingInterface()
    {
      $filesNameList = array();
      $glob_key = '/var/www/html/tmpLobotInterface/TBA_BHKSKA*';
      $loggerMessage = "Finding files : " . $glob_key;
      $this->logger->info($loggerMessage);

      if ($handle = opendir('/var/www/html/tmpLobotInterface')) {
            while (false !== ($entry = readdir($handle))) 
            {
              if ($entry != "." && $entry != "..") {
              }
            }
            closedir($handle);
        }

      foreach (glob($glob_key) as $filename) 
      {
          $loggerMessage = "Found file " . basename($filename);
          $this->logger->info($loggerMessage);
          array_push($filesNameList,basename($filename));
      }
      $fullContent = "";
      $index = 0;
      $maxIndex = count($filesNameList);
      foreach ($filesNameList as $filename) 
      {
        $currentFilePath = "/var/www/html/tmpLobotInterface/" . $filename;
        $myfile = fopen($currentFilePath, "r") or die("Unable to open file!");
        $newFilePath = "/var/www/html/tmpLobotInterface/Bin/" . $filename;
        // Output one line until end-of-file
        while(!feof($myfile)) {
          $content = fgets($myfile);
          $fullContent .= $content;
          //echo $content . "\n";
        }
        $index++;
        if($index < $maxIndex)
        {
          $fullContent .= "\r\n";
        }
        fclose($myfile);
        rename($currentFilePath, $newFilePath);
      }

      $this->lobotOrderTsvBuilder->BuildTsvFile($fullContent,true);
    }
}
