<?php
namespace TBA\LOBOT\Controller\Page;
use \Psr\Log\LoggerInterface;

class TbaReadOrderShippingInformation extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $orderFactory;
    protected $logger;

    public function __construct(LoggerInterface $logger,
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
       \Magento\Sales\Model\OrderFactory $orderFactory)
    {
       $this->orderFactory = $orderFactory;

       parent::__construct($context);
       $this->logger = $logger;
       $this->resultJsonFactory = $resultJsonFactory;
    }

    public function execute()
    {
      $result = $this->resultJsonFactory->create();
      $data = ['message' => 'Hello world!'];
      echo 'TbaReadOrderShippingInformation Start' . "\r\n";

      $filename = '/var/www/html/tmpLobotInterface/OrderShippingInformation.csv';

      $file = fopen($filename,"r");
      while(! feof($file))
      {
          $content = fgetcsv($file);
          $pad_length = 9;
          $pad_char = 0;
          $str_type = 'd'; // treats input as integer, and outputs as a (signed) decimal number
          $format = "%{$pad_char}{$pad_length}{$str_type}"; // or "%04d"
          $targetId = sprintf($format, $content[0]);
          $targetTN = $content[1];
          $targetSD = $content[2];

          $orderTar = $this->orderFactory->create()->loadByIncrementId($targetId);
          if ($orderTar->getId()){
                $orderTar->setData('trackingNumber',$targetTN);
                $orderTar->setData('shippingDate',$targetSD);
                $orderTar->setState(\Magento\Sales\Model\Order::STATE_COMPLETE);
                $orderTar->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE);
                $orderTar->save();

                $this->logger->info("TbaReadOrderShippingInformation complete Order : " . $orderTar->getIncrementId() . " : with TN : " . $orderTar->getData('trackingNumber'));
                $this->logger->info("TbaReadOrderShippingInformation complete Order : " . $orderTar->getIncrementId() . " : with SD : " . $orderTar->getData('shippingDate'));
          }
          else
          {
            echo 'TbaReadOrderShippingInformation not found $content[0] : ' . $targetId . "\r\n";
            echo "====================================================================================". "\r\n";
          }
      }

      fclose($file);

      return $result->setData($data);
    }
}