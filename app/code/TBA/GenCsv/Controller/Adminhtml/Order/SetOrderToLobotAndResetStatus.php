<?php

namespace TBA\GenCsv\Controller\Adminhtml\Order;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
//use Magento\Sales\Api\OrderManagementInterface;
use \Psr\Log\LoggerInterface;
use Magento\Framework\View\Result\PageFactory;

class SetOrderToLobotAndResetStatus extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
  protected $logger;
  protected $_productCollectionFactory;
  protected $orderRepository;
  protected $productRepository;
  protected $lobotOrderTsvBuilder;

  public function __construct(
      Context $context,
      Filter $filter,
      CollectionFactory $collectionFactory,
      LoggerInterface $logger,
      \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
      \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
      \TBA\LOBOT\Controller\LobotHelper\LobotOrderTsvBuilder $lobotOrderTsvBuilder,
      \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
  ) {
      parent::__construct($context, $filter);
      $this->logger = $logger;
      $this->collectionFactory = $collectionFactory;
      $this->productRepository = $productRepository;
      $this->lobotOrderTsvBuilder = $lobotOrderTsvBuilder;
      $this->orderRepository = $orderRepository;
      $this->_productCollectionFactory = $productCollectionFactory;
  }

  protected function massAction(AbstractCollection $collection)
  {
    $loggerMessage = "SetOrderToLobotAndResetStatus START";
    $this->logger->info($loggerMessage);
    $loggerMessage = "SetOrderToLobotAndResetStatus collection getItems count is :: " . count($collection->getItems());
    $this->logger->info($loggerMessage);

    foreach ($collection->getItems() as $order) {
      if($order->getId())
      {
        $orderId = $order->getId();
        $loadedOrder = $this->orderRepository->get($orderId);

        $this->AssignDataBean($loadedOrder);
      }
    } 
  }

  public function AssignDataBean($order)
  {
    $orderId = $order->getId();
    $status = $order->getStatus();

    $order->setData('trackingNumber',"");
    $order->setData('shippingDate',"");
    $order->setStatus("Order_Received");
    $order->save();

    $loggerMessage = 'SetOrderToLobotAndResetStatus Order ID is :: ' . $orderId;
    $this->logger->info($loggerMessage);
    $loggerMessage = 'SetOrderToLobotAndResetStatus Order Status is :: ' . $status;
    $this->logger->info($loggerMessage);

    $taxId = "Tax ID : 0105559143340";
    $kadokawaName = "KADOKAWA AMARIN COMPANY LIMITED";
    $kadokawaAdress1 = "378 Chaiyaphruk Road,";
    $kadokawaAdress2 = "Taling Chan, Bangkok 10170";
    $kadokawaAdress3 = "TEL. 02-434-0333";

    $this->logger->info('SetOrderToLobotAndResetStatus ---> TargetOrder id : ' . $orderId);
    $customerId = $order->getCustomerId();

    $shippingAddressObj = $order->getShippingAddress();
    $telephone = "None";
    $clientName = "None";
    $orderDeliveryPostCode = "None";
    $taxPrice = 0;
    $grandTotal = 0;

    if($shippingAddressObj)
    {
      $shippingAddressArray = $shippingAddressObj->getData();
      $telephone = $shippingAddressArray['telephone'];
      $clientName = $shippingAddressArray['firstname'] . ' ' . $shippingAddressArray['lastname'];

      $company = isset($shippingAddressArray['company']) ? $shippingAddressArray['company'] : "";
      $address1 = ' ' . $shippingAddressArray['street'];
      $address2 = ' ' . $shippingAddressArray['city'] . ' ' . $shippingAddressArray['region'];
      $address3 = ' ' . $shippingAddressArray['postcode'];

      $address = $company . $address1 . $address2 . $address3;

      $orderDeliveryPostCode = $shippingAddressArray['postcode'];
    }

    $products = array();
    $productsForCheck = array();
    $orderItems = $order->getAllItems();

    foreach ($orderItems as $product)
    {
      $productData = $this->productRepository->get($product->getSku());

      $loggerMessage = 'SetOrderToLobotAndResetStatus Try to get product id :: ' . $productData->getId() . ' ---> Name is :: ' . $productData->getName();
      $this->logger->info($loggerMessage);

      $stockStatus = $productData->getData('stock_status');
      $loggerMessage = 'SetOrderToLobotAndResetStatus Found product sku :: ' . $productData->getSku() . ' ---> Status is :: ' . $stockStatus;
      $this->logger->info($loggerMessage);

      $productsForCheck[$product->getSku()] = $product->getData('qty_ordered');

   
      $barcode = $productData->getData('barcode');

      $loggerMessage = 'SetOrderToLobotAndResetStatus Product is ready, Tax is :: ' . $product->getTaxAmount();
      $this->logger->info($loggerMessage);

      $taxPrice += $product->getTaxAmount();
      $itemPrice = $product->getData('price');
      $itemQty = $product->getData('qty_ordered');
      $grandTotal += $itemPrice * $itemQty;

      $productDataBean = array(
       "record_indicator" => 'D',
       "item_number" => $product->getSku(),
       "product_number" => $barcode,
       "catagories" => $product->getCategoryIds(),
       "product_name" => $product->getName(),
       "price" => $itemPrice,
       "quantity" => $itemQty,
       );
      array_push($products,$productDataBean);
    }

    $grandTotal += $order->getShippingAmount();
    $reward_spend = $order->getRewardsSpentAmount();
    if(empty($reward_spend))
      $reward_spend = 0;

    $orderDataBean = array(
     'record_indicator' => 'H',
     "r3" => "3001",
     //"unique_number" => $orderId,
     "unique_number" => $order->getIncrementId(),
     //"order_number" => $orderId,
     "order_number" => $order->getIncrementId(),
     "order_serial_number" => $order->getIncrementId(),
     "order_date" => date('Ymd',strtotime($order->getCreatedAt())),
     "order_member_number" => $customerId,
     "order_name" => $clientName,
     "order_order_address1" => $address1,
     "order_order_address2" => $address2,
     "order_order_address3" => $address3,
     "order_delivery_post_code" => $orderDeliveryPostCode,
     "order_phone_number" => $telephone,
     "destination_name" => $clientName,
     "delivery_address" => $address,
     "delivery_address1" => $address1,
     "delivery_address2" => $address2,
     "delivery_address3" => $address3,
     "destination_phone_number" => $telephone,
     "name_of_client" => $taxId,
     "name_of_client_local" => $kadokawaName,
     "client_address1" => $kadokawaAdress1,
     "client_address2" => $kadokawaAdress2,
     "client_address3" => $kadokawaAdress3,
     "client_address1_local" => $kadokawaAdress1,
     "client_address2_local" => $kadokawaAdress2,
     "client_address3_local" => $kadokawaAdress3,
     "requestor_phone_number" => $telephone,
     "shipping_amount" => $order->getShippingAmount(),
     "reward_spend" => $reward_spend,
     "discount_amount" => $order->getDiscountAmount(),
     "order_tax" => $taxPrice,
     );

    $orderBean = array(
      "header" => $orderDataBean,
      "detials" => $products
    );
    
    $tsvFilePath = $this->lobotOrderTsvBuilder->OrderToLobotTsvBuilder($orderBean);
    $loggerMessage = 'SetOrderToLobotAndResetStatus Order not contain PreOrder and send to Lobot :: ' . $orderId;
    $this->logger->info($loggerMessage);
  }
}