<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   3.0.7
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */


namespace Mirasvit\RewardsCheckout\Model\Checkout;

use Mirasvit\Rewards\Model\Purchase;

/**
 * Class TotalsInformationManagement
 */
class Rewards implements \Mirasvit\Rewards\Api\RewardsInterface
{
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;
    /**
     * @var \Mirasvit\Rewards\Model\Data\RewardsFactory
     */
    private $rewardsDataFactory;
    /**
     * @var \Mirasvit\Rewards\Helper\Balance
     */
    private $rewardsBalance;
    /**
     * @var \Mirasvit\Rewards\Helper\Data
     */
    private $rewardsData;
    /**
     * @var \Mirasvit\Rewards\Helper\Purchase
     */
    private $rewardsPurchase;
    /**
     * @var \Mirasvit\Rewards\Helper\Checkout
     */
    private $rewardsCheckout;

    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Mirasvit\Rewards\Model\Data\RewardsFactory $rewardsDataFactory,
        \Mirasvit\Rewards\Helper\Balance $rewardsBalance,
        \Mirasvit\Rewards\Helper\Checkout $rewardsCheckout,
        \Mirasvit\Rewards\Helper\Data $rewardsData,
        \Mirasvit\Rewards\Helper\Purchase $rewardsPurchase
    ) {
        $this->customerRepository = $customerRepository;
        $this->request            = $request;
        $this->quoteRepository    = $quoteRepository;
        $this->rewardsDataFactory = $rewardsDataFactory;
        $this->rewardsBalance     = $rewardsBalance;
        $this->rewardsData        = $rewardsData;
        $this->rewardsPurchase    = $rewardsPurchase;
        $this->rewardsCheckout    = $rewardsCheckout;
    }

    /**
     * {@inheritdoc}
     */
    public function update($shippingCarrier = '', $shippingMethod = '', $paymentMethod = '')
    {
        $result = [];
        $result['chechoutRewardsIsShow']         = 0;
        $result['chechoutRewardsPoints']         = 0;
        $result['chechoutRewardsPointsMax']      = 0;
        $result['chechoutRewardsPointsSpend']    = 0;
        $result['chechoutRewardsPointsAvailble'] = 0;
        if (($purchase = $this->rewardsPurchase->getPurchase()) && $purchase->getQuote()->getCustomerId()) {
            $this->refreshPoints($purchase, $shippingCarrier, $shippingMethod, $paymentMethod);
            if ($purchase->getEarnPoints()) {
                $result['chechoutRewardsPoints'] = $this->rewardsData->formatPoints($purchase->getEarnPoints());
            }
            if ($point = $purchase->getSpendPoints()) {
                $result['chechoutRewardsPointsSpend'] = $this->rewardsData->formatPoints($point);
                $result['chechoutRewardsPointsUsed']  = $point;
            }
            $quote = $purchase->getQuote();
            $result['chechoutRewardsPointsAvailble'] = $this->rewardsData->formatPoints(
                $purchase->getCustomerBalancePoints($quote->getCustomerId())
            );
            $result['chechoutRewardsPointsMax'] = $purchase->getMaxPointsNumberToSpent();
            $result['chechoutRewardsIsShow']    = $purchase->getEarnPoints() > 0;
        }

        $rewards = $this->rewardsDataFactory->create();
        $rewards->setData($result);

        return $rewards;
    }

    /**
     * @param Purchase $purchase
     * @param string   $shippingCarrier
     * @param string   $shippingMethod
     * @param string   $paymentMethod
     * @return void
     */
    private function refreshPoints(Purchase $purchase, $shippingCarrier, $shippingMethod, $paymentMethod)
    {
        $quote = $purchase->getQuote();
        if (!$quote->getIsVirtual() &&
            empty(trim($quote->getShippingAddress()->getShippingMethod(), '_')) &&
            !empty($shippingCarrier) && !empty($shippingMethod)
        ) {
            $quote->getShippingAddress()->setCollectShippingRates(true)->setShippingMethod(
                $shippingCarrier . '_' . $shippingMethod
            );
            $quote->setCartShippingCarrier($shippingCarrier);
            $quote->setCartShippingMethod($shippingMethod);
        }
        if ($paymentMethod) {
            $address = $quote->getShippingAddress();
            if ($quote->getItemVirtualQty() > 0) {
                $address = $quote->getBillingAddress();
            }
            if (!$address->getPaymentMethod()) {
                $address->setPaymentMethod($paymentMethod);
            }
        }
        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
    }

    /**
     * {@inheritdoc}
     */
    public function apply($cartId, $pointsAmount)
    {
        $purchase = $this->rewardsPurchase->getByQuote($cartId);
        if (empty($purchase->getQuote()) || !is_object($purchase->getQuote())) {
            /* @var $quote \Magento\Quote\Model\Quote */
            $quote = $this->quoteRepository->getActive($cartId);
            $purchase->setQuote($quote);
        }
        $this->request->setParams(['points_amount' => $pointsAmount]);

        return $this->rewardsCheckout->processApiRequest($purchase)['success'];
    }

    /**
     * {@inheritdoc}
     */
    public function getBalance($customerId)
    {
        $this->customerRepository->getById($customerId); // validate customer ID
        return $this->rewardsBalance->getBalancePoints($customerId);
    }

    /**
     * {@inheritdoc}
     */
    public function getBalances()
    {
        return $this->rewardsBalance->getAllBalances();
    }
}
