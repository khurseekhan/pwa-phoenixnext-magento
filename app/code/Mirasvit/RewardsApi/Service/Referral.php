<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   3.0.7
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\RewardsApi\Service;

use Mirasvit\Rewards\Model\Config;
use Mirasvit\Rewards\Model\ReferralFactory;
use Mirasvit\Rewards\Model\ResourceModel\ReferralLink\CollectionFactory;

class Referral
{
    private $referralFactory;
    private $referralLinkCollectionFactory;

    public function __construct(
        ReferralFactory $referralFactory,
        CollectionFactory $referralLinkCollectionFactory
    ) {
        $this->referralFactory               = $referralFactory;
        $this->referralLinkCollectionFactory = $referralLinkCollectionFactory;
    }

    /**
     * @param int $customerId
     * @return string
     */
    public function getReferralCode($customerId)
    {
        $link = $this->referralLinkCollectionFactory->create()
            ->addFieldToFilter('customer_id', $customerId)
            ->getFirstItem();
        //if we haven't generated link, create it
        if (!$link->getId()) {
            $link->createReferralLinkId($customerId);
        }

        return $link->getReferralLink();
    }

    /**
     * @param int    $customerId
     * @param string $code
     * @param int    $referrerCustomerId
     * @param int    $storeId
     * @return int
     */
    public function addReferral($customerId, $code, $referrerCustomerId, $storeId)
    {
        $result      = 0;
        $refererCode = $this->getReferralCode($referrerCustomerId);
        if ($refererCode == $code) {
            $referral = $this->referralFactory->create()
                ->setCustomerId($referrerCustomerId)
                ->setNewCustomerId($customerId)
                ->setStatus(Config::REFERRAL_STATUS_SIGNUP)
                ->setStoreId($storeId)
                ->save();
            $result = $referral->getId();
        }

        return $result;
    }
}